# angular_gulp
This is a simple app in angularjs that lets you search and sort for gulp plugins with a few more advanced options such as github stars, forks, links to the plugin.  It uses angularjs, jquery and bootstrap 3.

Check it out at http://gulp-plugins.learnler.com

## Authors

[Gaurav Chaturvedi](https://github.com/gchaturvedi)

[Erik Dasque](https://github.com/edasque)
